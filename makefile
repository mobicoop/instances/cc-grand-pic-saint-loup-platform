

blueDark:=$(shell tput setaf 21)
blue:=$(shell tput setaf 33)

ifeq ($(shell uname),Darwin)
  os=darwin
else
  os=linux
endif

install:
	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache ;\

	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache;\

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)cc-grand-pic-saint-loup-platform ($(os)): Installing node deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker compose -f docker-compose-builder-$(os).yml run --rm install
	@make -s install-vendor

install-vendor:

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)cc-grand-pic-saint-loup-platform ($(os)): Installing php deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker compose -f docker-compose-builder-$(os).yml run --rm install-vendor


fixtures:
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)cc-grand-pic-saint-loup-platform ($(os)): Generating fixtures...)
	$(info $(blue)------------------------------------------------------$(reset))
	@docker compose -f docker-compose-builder-$(os).yml run --rm fixtures

start:
	$(info cc-grand-pic-saint-loup-platform ($(os)): Starting cc-grand-pic-saint-loup-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml up -d
 
stop:
	$(info cc-grand-pic-saint-loup-platform ($(os)): Stopping cc-grand-pic-saint-loup-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml stop 

status:
	@docker ps -a | grep cc-grand-pic-saint-loup-platform_platform
	@docker ps -a | grep cc-grand-pic-saint-loup-platform_db
 
restart:
	$(info cc-grand-pic-saint-loup-platform ($(os)): Restarting cc-grand-pic-saint-loup-platform environment containers.)
	@make -s stop
	@make -s start

reload:
	$(info Make ($(os)): Restarting cc-grand-pic-saint-loup-platform environment containers.)
	@make -s stop
	@make -s remove
	@make -s start

remove:
	$(info cc-grand-pic-saint-loup-platform ($(os)): Stopping cc-grand-pic-saint-loup-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml down -v 
 
clean:
	@make -s stop
	@make -s remove
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)Drop all deps + containers + volumes)
	$(info $(blue)------------------------------------------------------$(reset))
	sudo rm -rf node_modules vendor

logs: 
	$(info $(blueDark)------------------------------------------------------)
	$(info $(blueDark)cc-grand-pic-saint-loup-platform+DB Logs)
	$(info $(blueDark)------------------------------------------------------$(reset))
	@docker logs -f cc-grand-pic-saint-loup-platform

go-platform:
	@docker exec -it cc-grand-pic-saint-loup-platform zsh